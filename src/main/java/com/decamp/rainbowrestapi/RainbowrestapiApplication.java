package com.decamp.rainbowrestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RainbowrestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RainbowrestapiApplication.class, args);
	}

}
